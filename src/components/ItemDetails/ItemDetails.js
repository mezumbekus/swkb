import React,{Component} from 'react';
import Spinner from "../spinner/Spinner";
import './ItemDetails.css';

const Record = ({item,field,label}) => {
    return (
        <li className="list-group-item">
            <span>{label}: </span>
            <span>{item[field]}</span>
        </li>
    );
};

export {
    Record
}
export default class ItemDetails extends Component{
    state = {
        item: null,
        image: null
    };

    componentDidMount() {
        this.updateItem();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.itemId != this.props.itemId)
            this.updateItem();
    }

    updateItem(){
        const {itemId,getData,getImageUrl} = this.props;
        if(!itemId)
            return;
        getData(itemId)
            .then((item)=>{
                this.setState({
                    item,
                    image: getImageUrl(itemId)
                });
            })
    }

    render(){

        const {item,image} = this.state;

        if(!item){
            return <Spinner></Spinner>
        }
        return (
            <div className="item-details">
                <div className="item-image">
                    <img src={image} alt=""/>
                </div>
            <div className="item-body">
                <ul>
                    {
                        React.Children.map(this.props.children,(child,idx)=>{
                            return React.cloneElement(child,{item});
                        })

                    }
                </ul>
            </div>
            </div>
        );
    }
}
