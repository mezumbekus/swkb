import React,{Component} from 'react';
import ItemList from "../item-list/ItemList";
import SwapiService from "../../services/SwapiService";
import ErrorWrapper from "../error/ErrorWrapper";
import {withRouter} from 'react-router-dom';

class PlanetPage extends Component{

    swapiService = new SwapiService();

    renderItem(item) {
        return (<span>
                        {item.planetName}
                </span>
        );
    };

    render() {

        const itemList = (<ItemList onItemSelected={(itemId)=>this.props.history.push(`/planet/${itemId}`)}
                                    getData={this.swapiService.getAllPlanets}
                                    renderItem={this.renderItem}
        ></ItemList>);

        return(
            <ErrorWrapper>
                {itemList}
            </ErrorWrapper>
        );
    }
}
export default withRouter(PlanetPage);
