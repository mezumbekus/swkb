import React,{Component} from 'react';
import {Link} from 'react-router-dom';

import './AppHeader.css';
export default class AppHeader extends Component {
    render() {
        return  (
          <div className="header">
              <h3 className="header-title">StarWars KB</h3>
              <ul className="header-menu">
                <Link to="/people">People</Link>
                <Link to="/starship">Starships</Link>
                <Link to="/planet">Planets</Link>
              </ul>
          </div>
        );
    }
}
