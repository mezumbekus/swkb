import React,{Component} from 'react';
import './App.css';
import AppHeader from "../app-header/AppHeader";
import RandomPlanet from "../random-planet/RandomPlanet";
import PersonPage from "../person-page/PersonPage";
import StarshipPage from "../starship-page/StarshipPage";
import PlanetPage from "../PlanetPage/PlanetPage";
import {BrowserRouter as Router,Route} from 'react-router-dom';
import PlanetDetails from "../sub-components/planet-details";
import PersonDetails from "../sub-components/person-details";
import StarshipDetails from "../sub-components/starship-details";

export default class App extends Component{

    render() {

      return (
          <Router>
          <div className="App">
              <AppHeader></AppHeader>
              <div className="container">
                  <RandomPlanet></RandomPlanet>
                  <Route path="/" exact render={()=><h2>Star Wars База знаний</h2>}></Route>
                  <Route path="/people" exact render={()=><h2>People</h2>}></Route>
                  <Route path="/people" exact component={PersonPage}></Route>
                  <Route path="/people/:id"
                         render={({match}) => {
                             return <PersonDetails id={match.params.id}/>
                         }}
                  ></Route>
                  <Route path="/starship" exact render={()=><h2>Starships</h2>}></Route>
                  <Route path="/starship" exact component={StarshipPage}></Route>
                  <Route path="/starship/:id"
                         render={({match}) => {
                             return <StarshipDetails id={match.params.id}/>
                         }}
                  ></Route>
                  <Route path="/planet" exact render={()=><h2>Planets</h2>}></Route>
                  <Route path="/planet/:id"
                         render={({match}) => {
                            return <PlanetDetails planetId={match.params.id}/>
                         }}
                  ></Route>
                  <Route path="/planet" component={PlanetPage} exact></Route>
              </div>
          </div>
          </Router>
      )
  }
}
