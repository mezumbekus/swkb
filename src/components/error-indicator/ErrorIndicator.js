import React,{Component} from 'react';
import './ErrorIndicator.css';

export default class ErrorIndicator extends Component{
    render(){
        return (
            <div className="error-indicator">
                <h2>Something went wrong!</h2>
            </div>
        );
    }
}