import React,{Component} from 'react';
import ErrorIndicator from "../error-indicator/ErrorIndicator";

export default class ErrorWrapper extends Component{
    state = {
        hasErrors: false
    };

    componentDidCatch() {
        this.setState({
            hasErrors: true
        })
    }

    render() {
        if(this.state.hasErrors){
            return <ErrorIndicator></ErrorIndicator>
        }
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}
