import React,{Component} from 'react';
import './ItemList.css';
import Spinner from "../spinner/Spinner";

export default class ItemList extends Component{
    state = {
        itemList: null
    };

    componentDidMount = ()=> {
        const getData = this.props.getData;
            getData()
            .then((body)=>{
                this.setState({
                    itemList: body
                });
            });
    };

    getItemList(arr){
        return arr.map((item) => {
           return (
               <li className="list-group-item"
                key={item.id}
                   onClick={()=> this.props.onItemSelected(item.id)}
               >
                   {this.props.renderItem(item)}
               </li>
           )
        });
    }

    render(){
        const {itemList} = this.state;
        if(!itemList){
            return <Spinner></Spinner>;
        }
        return (
        <ul className="item-list list-group">
            {this.getItemList(itemList)}
        </ul>
        );
    }
}
