import React,{Component} from 'react';
import ItemList from "../item-list/ItemList";
import SwapiService from "../../services/SwapiService";
import ErrorWrapper from "../error/ErrorWrapper";
import {withRouter} from 'react-router-dom';

class PersonPage extends Component{

    swapiService = new SwapiService();
    renderItem(item) {
        return (
            <span>
                    {item.name}
            </span>
        );
    };

    render() {
        const itemList = (
            <ItemList
                                    onItemSelected={(id) => this.props.history.push(`/people/${id}`)}
                                    getData={this.swapiService.getAllPeople}
                                    renderItem={this.renderItem}
            />);
        return(
           <ErrorWrapper>
                {itemList}
            </ErrorWrapper>
    );
    }
}
export default withRouter(PersonPage);
