import React,{Component} from 'react';
import './RandomPlanet.css';
import SwapiService from "../../services/SwapiService";
import Spinner from "../spinner/Spinner";
import ErrorIndicator from "../error-indicator/ErrorIndicator";

export default class RandomPlanet extends Component {

    state = {
        planet: {},
        loading: true
    };
    swapiService = new SwapiService();

    componentDidMount() {
        this.interval = setInterval(this.updatePlanet,50000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }


    onPlanetLoaded(planet){
        this.setState({
            planet,
            loading: false,
            err: false
        });
    }
    updatePlanet = () =>{
        const id = Math.floor(Math.random()*24+3);
        this.swapiService
            .getPlanet(id)
            .then((planet)=>{
               this.onPlanetLoaded(planet);
            })
            .catch(this.onError);
    }

    onError = () => {
        this.setState({
            err: true,
            loading: false
        })
    }

    render() {
        const {planet,loading,err} = this.state;
        const showData = !(loading || err);
        const error = err ? <ErrorIndicator></ErrorIndicator> : null;
        const spinner = loading ? <Spinner></Spinner> : null;
        const content = showData ? <PlanetView planet={planet}></PlanetView> : null;
        return  (
            <div className="random-planet">
                {error}
                {spinner}
                {content}
            </div>
        );
    }
}


const PlanetView = ({planet}) => {
        const {id,planetName,population,rotationPeriod,diameter} = planet;

        const getImageUrl = (planetId) => {
            return `https://starwars-visualguide.com/assets/img/planets/${planetId}.jpg`;

        };
        return (
            <React.Fragment>
                <img src={getImageUrl(id)} alt="" className="planet-image"/>
                <div className="planet-desc">
                    <h3>{planetName}</h3>
                    <p>
                        Population <span className="planet-population">{population}</span>
                    </p>
                    <p>Rotation period <span className="planet-period">{rotationPeriod}</span></p>
                    <p>Diameter <span className="planet-diameter">{diameter}</span></p>
                </div>
            </React.Fragment>
        );
}
