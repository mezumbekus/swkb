import React,{Component} from 'react';
import ItemList from "../item-list/ItemList";
import SwapiService from "../../services/SwapiService";
import {withRouter} from 'react-router-dom';

class StarshipPage extends Component{

    swapiService = new SwapiService();

    renderItem(item) {
        return (<span>
                    {item.name} {item.length}
                </span>);
    };

    render() {
        const itemList = (
            <ItemList onItemSelected={(id) => this.props.history.push(`/starship/${id}`)}
                      getData={this.swapiService.getAllStarships}
                      renderItem={this.renderItem}
            ></ItemList>
        );

        return <div>{itemList}</div>
    }
}
export default withRouter(StarshipPage);
