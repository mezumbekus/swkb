import React from 'react';
import ItemDetails, {Record} from "../ItemDetails/ItemDetails";
import SwapiService from "../../services/SwapiService";


export default class PersonDetails extends React.Component
{
    swapiService = new SwapiService();
    render() {
        const id = this.props.id;
        const {getPerson,getPersonImage} = this.swapiService;
        return (
            <ItemDetails
                itemId={id}
                getData={getPerson}
                getImageUrl={getPersonImage}
            >
                <Record field="gender" label="Sex"></Record>
                <Record field="age" label="Age"></Record>
            </ItemDetails>

        );
    }
};
