import React from 'react';
import ItemDetails, {Record} from "../ItemDetails/ItemDetails";
import SwapiService from "../../services/SwapiService";



export default class PlanetDetails extends React.Component
{
    swapiService = new SwapiService();

    render() {
        const {getPlanet,getPlanetImage} = this.swapiService;
        const id = this.props.planetId;
        return (
            <ItemDetails
                itemId={id}
                getData={getPlanet}
                getImageUrl={getPlanetImage}
            >
                <Record field="planetName" label="Name"></Record>
            </ItemDetails>

        );
    }
};
