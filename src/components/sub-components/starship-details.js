import React from 'react';
import ItemDetails, {Record} from "../ItemDetails/ItemDetails";
import SwapiService from "../../services/SwapiService";


export default class StarshipDetails extends React.Component
{
    swapiService = new SwapiService();
    render() {
        const id = this.props.id;
        const {getStarship,getStarshipImage} = this.swapiService;
        return (
            <ItemDetails
                itemId={id}
                getData={getStarship}
                getImageUrl={getStarshipImage}
            >
                <Record field="name" label="Name"></Record>
                <Record field="length" label="Length"></Record>
            </ItemDetails>

        );
    }
};
