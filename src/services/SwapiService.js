export default class SwapiService {

    _apiBase = 'https://swapi.co/api';
    _baseImageUrl = 'https://starwars-visualguide.com/assets/img/';

    getPersonImage = (id) => {
        return `${this._baseImageUrl}characters/${id}.jpg`;
    };

    getPlanetImage = (id) => {
        return `${this._baseImageUrl}planets/${id}.jpg`;
    }

    getStarshipImage = (id) => {
        return `${this._baseImageUrl}starships/${id}.jpg`;
    }

    async getResource(url) {
        const fullUrl = this._apiBase+url;
        const res = await fetch(fullUrl);
        if(!res.ok) {
            throw new Error('Не смог получить страницу ' + fullUrl);
        }
        return await res.json();
    }

    getAllPeople = async () => {
        const result =  await this.getResource('/people');
        return result.results.map(result => {
            return this._transformPerson(result);
        });
    };
    getAllPlanets = async () => {
        const result =  await this.getResource('/planets');
        return result.results.map((planet)=> {
            return this._transformPlanet(planet);
        });
    };
    getAllStarships = async () => {
        const result =  await this.getResource('/starships');
        return result.results.map(result => {
            return this._transformStarship(result);
        });
    };

    getPerson = async (id) =>{
        const person = await this.getResource(`/people/${id}`);
        return this._transformPerson(person);
    };
    getPlanet = async (id) => {
        const planet = await this.getResource(`/planets/${id}`);
        return this._transformPlanet(planet);
    };
    getStarship = async (id) => {
        const ship = await this.getResource(`/starships/${id}`);
        return this._transformStarship(ship);
    };

    _extractId(item) {
        return item.url.match(/\/(\d+)\/$/)[1];
    }
    _transformPlanet(planet){
        return {
            id: this._extractId(planet),
            planetName: planet.name,
            population: planet.population,
            diameter: planet.diameter,
            rotationPeriod: planet.rotation_period
        }
    }

    _transformPerson(person) {
        return {
            id: this._extractId(person),
            name: person.name,
            age: person.birth_year,
            gender: person.gender,
            height: person.height,
        }
    }

    _transformStarship(ship) {
        return {
            id: this._extractId(ship),
            model: ship.model,
            name: ship.name,
            length: ship.length
        }
    }
}
